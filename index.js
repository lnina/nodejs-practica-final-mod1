const path = require('path');

const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const bodyParser =  require('body-parser');
const {PersonaRoute} = require ('./routes');

const ejs = require('ejs');

const mongoose = require('mongoose');
//const {db} = require ('./config');
mongoose.connect('mongodb://localhost:27017/practica-final', {useNewUrlParser: true, useUnifiedTopology: true});

//app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.set('views', path.join(__dirname, 'views/'));
app.set('view engine', 'ejs');


app.use(PersonaRoute);

app.get('/',(req,res)=>{
	res.status(200).json({
		'estado':true,
		'mensaje':'correcto'
	});
});

app.listen(port,()=>{
	console.log('Servicio iniciado en el puerto 3000');
});