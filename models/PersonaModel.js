const mongoose = require ('mongoose');
const {Schema} = mongoose;

const PersonaSchema = new Schema({
	nombre: String,
	apellidoPaterno: String,
	apellidoMaterno: String,
	numeroDocumento: String,
});

const Persona = mongoose.model('persona',PersonaSchema);

module.exports= Persona;
