const express = require('express');
const router = express.Router();
const {PersonaController} =require('../controllers');

router.get('/persona-list',		PersonaController.listPersonas);
router.post('/persona-create',	PersonaController.createPersona);
router.get('/persona-delete/:id',	PersonaController.deletePersona);
router.post('/persona-update/:id',	PersonaController.updatePersona);

router.get('/persona-form-create',	PersonaController.createFormPersona);
router.get('/persona-form-edit/:id',	PersonaController.editFormPersona);

//router.delete('/persona-delete/:id',	PersonaController.deletePersona);
//router.put('/persona-update/:id',	PersonaController.updatePersona);

module.exports = router;