const {PersonaModel} = require('../models');
const ejs = require('ejs');
const fs = require('fs');


function response(_finalizado,_mensaje,_datos)
{
	const res = {
		'finalizado': _finalizado,'mensaje': _mensaje,'datos': _datos,
	};
	return res;
}

const listPersonas = async (req,res)=>
{
	try
	{
		const personas = await PersonaModel.find();
		//const html = fs.readFileSync('./views/persona-list.ejs','utf-8');
	  //res.status(200).json(response(true,'Personas listadas correctamente',[personas]));
	  //const htmlRender = ejs.render('persona-list',{personas:personas});
	  //res.status(200);
	  res.render('persona-list',{personas:personas});

	}
	catch(error)
	{
		console.error(error);
		//res.status(404).json(response(false,error,[]));
		res.render('persona-error', {error:error});
	}
};

const createPersona = async (req,res)=>
{
	try
	{
		const personaTmp = req.body;
		const {request_nombre,  request_paterno,  request_materno,  request_nroDocumento}=personaTmp;
		if(request_nombre.length==0 ||
			request_paterno.length == 0||
			request_materno.length == 0||
			request_nroDocumento.length == 0
		 )
			throw new Error('Datos vacios');

		const persona ={
			"nombre": request_nombre,
			"apellidoPaterno":request_paterno,
			"apellidoMaterno":request_materno,
			"numeroDocumento":request_nroDocumento,
		};
		const create = await PersonaModel.create(persona);
	  //res.status(200).json(response(true,'Persona creada correctamente',create));
	  res.redirect('/persona-list');
	}
	catch(error)
	{
		console.log(error);
		//res.status(404).json(response(false,error,[]));
		res.render('persona-error', {error:error});
	}
};

const updatePersona = async (req,res)=>
{
	try
	{
		const id = req.params.id;
		const personaTmp = req.body;
		const {request_nombre,  request_paterno,  request_materno,  request_nroDocumento}=personaTmp;
		if(request_nombre.length==0 ||
			request_paterno.length == 0||
			request_materno.length == 0||
			request_nroDocumento.length == 0
		 )
			throw new Error('Datos vacios');

		const persona ={
			"nombre": request_nombre,
			"apellidoPaterno":request_paterno,
			"apellidoMaterno":request_materno,
			"numeroDocumento":request_nroDocumento,
		};

		const update = await PersonaModel.findByIdAndUpdate(id,persona);
	  //res.status(200).json(response(true,'Persona actualizada correctamente',update));
	  res.redirect('/persona-list');
	}
	catch(error)
	{
		console.log(error);
		//res.status(404).json(response(false,error,[]));
		res.render('persona-error', {error:error});
	}
};

const deletePersona = async (req,res)=>
{
	try
	{
		const id = req.params.id;
		const deletes= await PersonaModel.findOneAndRemove({'_id' : id});
		//res.status(200).json(response(true,'Persona borrada correctamente',deletes));
		res.redirect('/persona-list');
	}
	catch(error)
	{
		console.log(error);
		//res.status(404).json(response(false,error,[]));
		res.render('persona-error', {error:error});
	}
};



//formularios

const createFormPersona = async (req,res)=>
{
	try
	{
		 res.render('persona-create-from');
	}
	catch(error)
	{
		console.error(error);
		//res.status(404).json(response(false,error,[]));
		res.render('persona-error', {error:error});
	}
};

const editFormPersona = async (req,res)=>
{
	const id=req.params.id;
	try
	{
		 const persona =await PersonaModel.findOne({'_id':id});
		 res.render('persona-edit-from',{persona:persona});
	}
	catch(error)
	{
		console.error(error);
		//res.status(404).json(response(false,error,[]));
		res.render('persona-error', {error:error});
	}
};

module.exports =
{
	listPersonas,
	createPersona,
	updatePersona,
	deletePersona,
	createFormPersona,
	editFormPersona,
};